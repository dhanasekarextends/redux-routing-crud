export const formItems = [
    {
        label: 'Profile Picture',
        key: 'selectedImage',
        type: 'file',
    }, {
        label: 'First Name',
        key: 'firstName',
        type: 'text',
    }, {
        label: 'Last Name',
        key: 'lastName',
        type: 'text',
    }, {
        label: 'DOB',
        key: 'dob',
        type: 'date',
    }, {
        label: 'Gender',
        key: 'gender',
        button: ['Male', 'Female'],
        type: 'radio',
    }, {
        label: 'Email Address',
        key: 'email',
        type: 'text',
    }, {
        label: 'Phone',
        key: 'phone',
        type: 'number',
    }, {
        label: 'Communication Address',
        key: 'commAddress',
        type: 'textarea',
    }, {
        label: 'Permanent Address',
        key: 'permAddress',
        type: 'textarea',
    }
]

export const tableRatios = ['5%', '10%', '10%', '8%', '5%', '17%', '10%', '18%', '18%', '4%'];

export const tableHeaders = ['Image', 'FirstName', 'LastName',
    'DOB', 'Gender', 'Email', 'Ph. Number', 'Address 1', 'Address 2', 'Actions']