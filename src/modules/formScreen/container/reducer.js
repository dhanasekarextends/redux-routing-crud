import { CHANGE } from "./actions";

export const initialState = {
    sameAdd: false,
    update: false,
    userDetails: {
        id: '',
        firstName: "",
        lastName: "",
        dob: "",
        gender: "Male",
        email: "",
        phone: "",
        commAddress: "",
        permAddress: "",
        selectedImage: ""
    },
    errorMsgs: {
        firstName: "",
        lastName: "",
        dob: "",
        email: "",
        phone: "",
        commAddress: "",
        permAddress: "",
        selectedImage: ""
    },
    row: [],
}

export const FormScreenReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE:
            return { ...state, [action.payload.name]: action.payload.value }
        default:
            return state;
    }
}