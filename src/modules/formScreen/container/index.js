import React, { Component } from 'react';
import InputField from '../components/inputField';
import '../../../styles/formStyle.css';
import { formItems } from '../../../seed/seed';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import * as FormScreenAction from './actions';

class FormScreen extends Component {

    render() {
        const { actionClick, fieldOnChange, checkedValue, FormScreenData, fileSelected } = this.props;
        let fields = formItems.map((item) => {
            return (
                <InputField id={item.key} onChange={(event) => fieldOnChange(event.target.value, item.key)}
                    label={item.label} type={item.type} button={item.button ? item.button : []}
                    onChangeRadio={event => checkedValue(event)} errorMsg={FormScreenData.errorMsgs[item.key]}
                     accept="image/*" value={FormScreenData.userDetails} 
                    onChangeImage={event => fileSelected(item.key, event)} checked={FormScreenData.userDetails.gender} />
            )
        });
        return (
            <div>
                <div className='main-container'>
                    {fields}
                </div>
                <span className='action-button' onClick={actionClick}>Create</span>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        FormScreenData: state.FormScreenReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        actionClick: FormScreenAction.handleActionClick,
        fieldOnChange: FormScreenAction.fieldOnChange,
        checkedValue: FormScreenAction.checkedValue,
        fileSelected: FormScreenAction.fileSelected,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(FormScreen);