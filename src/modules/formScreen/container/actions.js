export const CHANGE = "FORM_CHANGE"
export const handleChange = (name, value) => ({
    type: CHANGE, payload: { name: name, value: value }
})

export const fieldOnChange = (text, key) => {
    return async (dispatch, getState) => {
        const { userDetails } = getState().FormScreenReducer;
        userDetails[key] = text;
        dispatch(handleChange("userDetails", userDetails));
    }
}

export const checkedValue = (event) => {
    return async (dispatch, getState) => {
        const { userDetails } = getState().FormScreenReducer;
        userDetails.gender = event.target.value;
        dispatch(handleChange('userDetails', userDetails))
    }
}

export const fileSelected = (key, event) => {
    return async (dispatch, getState) => {
        const { userDetails } = getState().FormScreenReducer;
        userDetails.selectedImage = URL.createObjectURL(event.target.files[0]);
        let fileName = event.target.files[0].name;
        console.log(fileName)
        dispatch(handleChange('userDetails', userDetails))
    }
};

const validation = () => {
    return async (dispatch, getState) => {
        const { userDetails, errorMsgs } = getState().FormScreenReducer;
        let check = false;
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        Object.keys(userDetails).map(key => {
            switch (key) {
                case key:
                    errorMsgs[key] =
                        userDetails[key] === "" ? "*" + key + " field can't be blank" : "";
                    break;
                default:
            }
            if (errorMsgs[key] !== "" && key !== 'id') check = true;

            return 0;
        });

        if (userDetails.phone.length < 10 || userDetails.phone.length > 11) {
            errorMsgs["phone"] = "Enter a valid mobile number";
            check = true;
        }

        if (!re.test(String(userDetails.email).toLowerCase())) {
            errorMsgs["email"] = "Enter a valid Email Address";
            check = true;
        }

        if (userDetails.selectedImage.length === 0) {
            errorMsgs["selectedImage"] = "Select an image";
            check = true;
        }
        dispatch(handleChange("errorMsgs", errorMsgs));
        return check;
    }
};

const idAlignment = () => {
    return async (dispatch, getState) => {
        const row = getState().FormScreenReducer;
        for (let i = 0; i < row.length; i++) {
            row[i].id = i;
        }
        dispatch(handleChange("row", row));
    }
};

export const handleActionClick = () => {
    return async (dispatch, getState) => {
        let a = await dispatch(validation());
        if (!a) {
            const { row, userDetails, update } = getState().FormScreenReducer;
            update
                ? (row[userDetails.id] = userDetails)
                : row.push({ ...userDetails, id: row.length });
                console.log(userDetails)
            await Object.keys(userDetails).map((item, index)=> {
                return(
                    userDetails[item]=""
                )
            })
            dispatch(handleChange('userDetails', userDetails))
            dispatch(handleChange('row', row))
            dispatch(handleChange('update', update))

        }
    }
}

export const deleteOnClick = key => {
    return async (dispatch, getState) => {
        const { update, row } = getState().FormScreenReducer;
        if (update !== true) {
            row.splice(key, 1);

            if (row.length >= 0) {
                await dispatch(idAlignment());
            }
            dispatch(handleChange('row', row))
        }
    }
};

export const editOnClick = key => {
    return async (dispatch, getState) => {
        const { row } = getState().FormScreenReducer;
        const userDetails = row[key];
        const update = true;
        dispatch(handleChange('userDetails', userDetails));
        dispatch(handleChange('update', update));

    }
};