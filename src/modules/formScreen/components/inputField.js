import React from 'react';

const InputField = (props) => {
    console.log(props.id, props.value[props.id])
    let text = <input className='input-field' type={props.type} onChange={props.onChange}
        placeholder={props.label} value={props.value[props.id]} />;
    let textArea = <textarea rows='5' className='input-field' type={props.type} onChange={props.onChange}
        placeholder={props.label} value={props.value[props.id]} />;
    let image = <input className='input-field' type={props.type} onChange={(event) => props.onChangeImage(event)}
        placeholder={props.label} />;
    let radio = <div className={"radio-container"}>
        {props.button.map((item, index) => {
            return (
                <div key={index}>
                    <input type="radio" name="gender" value={item}
                        onChange={(event) => props.onChangeRadio(event)}
                        checked={props.checked === item} />
                    <label className='radio-label'>{item}</label>
                </div>
            )
        })}</div>

    return (
        <div className='input-container'>
            <label className='label'>{props.label}</label>
            {props.type === 'textarea' ? textArea : props.type === 'radio' ? radio : props.type === 'file' ? image : text}
            {props.errorMsg ? <div className="error-msg">{props.errorMsg}</div> : null}
        </div>
    );
};

export default InputField;