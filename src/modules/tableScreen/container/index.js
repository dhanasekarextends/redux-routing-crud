import React, { Component } from 'react';
import TableArea from '../components/tableArea';
import { bindActionCreators } from "redux";
import * as FormScreenAction from '../../formScreen/container/actions';
import { connect } from 'react-redux';

class TableScreen extends Component {
    render() {
        const { TableData, deleteOnClick, editOnClick } = this.props;
        return (
            <div>
                <TableArea row={TableData.row} deleteOnClick={(key) => deleteOnClick(key)} editOnClick={(key)=> editOnClick(key)}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        TableData: state.FormScreenReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        deleteOnClick: FormScreenAction.deleteOnClick,
        editOnClick: FormScreenAction.editOnClick,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(TableScreen);