import React, { Component } from "react";
import { tableRatios, tableHeaders } from '../../../seed/seed';

class TableArea extends Component {
  render() {
    let tableRatio = tableRatios.map((item, index) => {
      return (
        <col key={index}width={item} />
      )
    });
    let tableHeader = tableHeaders.map((item, index)=>{
      return(
        <th key={index}>{item}</th>
      )
    });
    let usersList = this.props.row.map(userDetails => {
      return (
        <tr key={userDetails.id}>
          <td><img className="selected-image" src={userDetails.selectedImage} /></td>
          <td>{userDetails.firstName}</td>
          <td>{userDetails.lastName}</td>
          <td>{userDetails.dob}</td>
          <td>{userDetails.gender}</td>
          <td>{userDetails.email}</td>
          <td>{userDetails.phone}</td>
          <td>{userDetails.commAddress}</td>
          <td>{userDetails.permAddress}</td>
          <td className="action-icons">
            <div className="icon-class">
              <i
                className="fa fa-pencil-square"
                aria-hidden="true"
                onClick={() => this.props.editOnClick(userDetails.id)}
              />
            </div>
            <div className="icon-class">
              <i
                className="fa fa-trash"
                aria-hidden="true"
                onClick={() => this.props.deleteOnClick(userDetails.id)}
              />
            </div>
          </td>
        </tr>
      );
    });
    return (
      <div>
        <table className="table-class">
          <colgroup>
            {tableRatio}
          </colgroup>
          <thead>
            <tr>
              {tableHeader}
            </tr>
          </thead>
          <tbody>{usersList}</tbody>
        </table>
      </div>
    );
  }
}

export default TableArea;
