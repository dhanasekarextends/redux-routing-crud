import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import FormScreen from './modules/formScreen/container/index';
import TableScreen from './modules/tableScreen/container/index';
import "font-awesome/css/font-awesome.min.css";
import { NavLink } from 'react-router-dom'


function App() {

  return (
    <Router>
      <div className="App">
        <ul>
          <li><NavLink to='/' activeStyle={{ color: 'red' }} exact>Home</NavLink></li>
          <li><NavLink to='/data-table' activeStyle={{ color: 'red' }} exact>dataTable</NavLink></li>
        </ul>
        <Route path='/' render={() => <FormScreen />} exact strict />
        <Route path='/data-table' render={() => <TableScreen />} exact strict />
      </div>
    </Router>
  );
}

export default App;
